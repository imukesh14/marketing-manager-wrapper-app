package marketingmanager.provenlogic.com.marketingmanager;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import im.delight.android.webview.AdvancedWebView;

public class MarketingManager extends AppCompatActivity implements AdvancedWebView.Listener{
    private AdvancedWebView marketingManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_marketing_manager);

        marketingManager = (AdvancedWebView) findViewById(R.id.marketing_manager);

        marketingManager.setListener(this, this);
        marketingManager.loadUrl("http://cleanup.tech/marketing-manager/#!/login");
    }


    @SuppressLint("NewApi")
    @Override
    protected void onResume() {
        super.onResume();
        marketingManager.onResume();
        // ...
    }

    @SuppressLint("NewApi")
    @Override
    protected void onPause() {
        marketingManager.onPause();
        // ...
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        marketingManager.onDestroy();
        // ...
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        marketingManager.onActivityResult(requestCode, resultCode, intent);
        // ...
    }

    @Override
    public void onBackPressed() {
        if (!marketingManager.onBackPressed()) { return; }
        // ...
        super.onBackPressed();
    }


    @Override
    public void onPageStarted(String url, Bitmap favicon) {

    }

    @Override
    public void onPageFinished(String url) {

    }

    @Override
    public void onPageError(int errorCode, String description, String failingUrl) {

    }

    @Override
    public void onDownloadRequested(String url, String suggestedFilename, String mimeType, long contentLength, String contentDisposition, String userAgent) {

    }

    @Override
    public void onExternalPageRequest(String url) {

    }
}
